# Changes in Bit&Black Composer Helper v1.0

## 1.0.1 2023-08-14

### Fixed

-   Fixed problem with search for `autoload.php` in WordPress installations.

## 1.0.0 2022-05-12

### Changed

-   PHP >=7.4 is now required.
-   Remove deprecations.