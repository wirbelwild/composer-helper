<?php

/**
 * Bit&Black Composer Helper.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\Composer\Tests;

use BitAndBlack\Composer\Composer;
use Composer\Autoload\ClassLoader;
use Composer\Autoload\MissingClass;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use TEST;

/**
 * Class ComposerTest
 *
 * @package BitAndBlack\Composer\Tests
 */
class ComposerTest extends TestCase
{
    public static function getCanFindClassData(): Generator
    {
        /**
         * If this returns true some day, this method will no longer be needed.
         */
        yield [
            class_exists(TEST::class, false),
            false,
        ];

        yield [
            Composer::classExists(TEST::class),
            true,
        ];

        yield [
            Composer::classExists(ClassLoader::class),
            true,
        ];

        yield [
            /** @phpstan-ignore-next-line */
            Composer::classExists(MissingClass::class),
            false,
        ];

        yield [
            /** @phpstan-ignore-next-line */
            Composer::classExists(\MissingNamespace\ClassLoader::class),
            false,
        ];
    }

    /**
     * Tests if classes ca be found.
     */
    #[DataProvider('getCanFindClassData')]
    public function testCanFindClass(bool $classExists, bool $classExistsExpected): void
    {
        self::assertSame(
            $classExistsExpected,
            $classExists
        );
    }
}
